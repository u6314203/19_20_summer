## [Deep learning using physiological data](https://gitlab.anu.edu.au/u6314203/19_20_summer)

#### Project description

Use physiological data during a driving simulator experiment to predict event labels

+ All together 8 event labels:      				
  >     Almost hit a pedestrian, Almost hit a vehicle,Almost hit an object, 
  >
  >     Hit a pedestrian, Hit a vehicle, Hit an object
  >
  >     Normal driving, Stopping
  
+ Available physiological signals

  > EyeTribe, BVP, EMG, GSR, IBI, TEM

Due to the unexpected mountain fire situation across Australia, the summer research has an early termination and this project is not completed finished. The early termination date is 01/12/2020 any by that date, the focus is using eyeTribe data (mainly pupil size) and the VR data is excluded(please see [Jinshuai's thesis](./papers/stage3/final_report_201810260052.pdf) for reason)



#### Raw Dataset

The raw dataset comes from [Jinshuai's Honours Project Experiment](https://anu365-my.sharepoint.com/:f:/g/personal/u5870682_anu_edu_au/EqEO0j5HSTpEpdEbT5xYmuEBKAd3VkrYHYZpDptig8c8GA?e=GXXdzi)

+ `AExp*.txt` - eyeTribe data; API available [Here](https://theeyetribe.com/dev.theeyetribe.com/dev.theeyetribe.com/api/index.html#cat_calib)

+ `153*.csv` -  VR data

+ `20180810*.txt`- Event labels



#### Contents

Please head to [this documentation](./folder_des.md).



#### Work has been done

1. From txt and csv file, read in the eyeTribe data and event labels

2. **Preprocessing**: 

   1. MinMax Normalization of pupil size on each individual 
   2. Data segmentation: prepare X and y(input and output dataset)

   + y: event labels encoded into integers
   + X: certain number of pieces of pupil size data recorded right before the timestamp associated with the event

3. Classification using various models(without hyperparameter tuning)

4. Evaluation using different metrics



#### Results and issues

##### Using raw signals

1. Experiment: Off-shelf models from sklearn(namely KNNClassifier, logistic classifier, SVM Classifier, DecisionTreeClassifier etc.) as well as a full-connected ANN built by pyTorch.
2. The mean accuracy is around 50%-70%. However this is not a good metric as among 8 classes, `normal driving` takes up to 70%, essentially all models are biased towards that and predicting every event to be normal driving. 
3. By switching the metrics to balanced accuracy, it drops to 10% - 13%. This a quite unbalanced dataset, but I am still in the process of attempting various techniques to deal with that at the moment of early termination.  One method I tried is to provide CrossEntropyLoss with a weight parameter within ANN, however it does improve much.

##### Using features constructed from raw signals

By early termination, only some simple statistical features are constructed and experimented with(namely mean, minmax, std, IQR, var, Kurtosis, skew). However using above mentioned models, the balanced accuracy does not improve and model is found to still predict every event to be "Normal driving".



#### Next steps

1. Continue experimenting with data augmentation techniques to deal with unbalanced dataset

2. Construct some more advanced features and do a feature selection before feeding into above mentioned models.

3. Apply smoothing techniques after normalization

4. Try some model hyperparameter tuning

   

#### Other thoughts

1. Date segmentation: Jinshuai mentioned he segment on the time(3 seconds around label), that is fine if features are constructed before feeding into model. However, if raw data is fed, it is a problem since input can be of different shape(there could be 50 records or 80 records or so within 3 seconds). So in section "using raw signals", I just take a fixed number of records before event. That might be a problem.
2. Since no statistical analysis is done before classification, now I am worrying pupil size may not be sufficient for classification. Maybe it is worthwhile to take stats analysis. And it is worth bring up that there are other signals such as GSR, BVP. Maybe we can consider incorporate them.



