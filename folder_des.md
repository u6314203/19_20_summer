#### Note: there are notations and highlights inside the papers

- cleaned dataset: a dataset cleaned by Jinshuai for his purpose(classify simulator configuration)

- papers: all relevant paper

  > - stage1: At the very beginning of summer research, papers used to familiarize myself of this field. They are previous works on analyzing physiological data.
  > - stage2: Papers read during the stage of deciding specific tasks.
  > - stage3: Papers directly useful for the final determined task.
  > - [Future]Imitation network & eye gaze: A future interest(though it might not be so related to this project)
- 'main' notebook - holds all relevant code including:

  > 1. load dataset
  > 2. preprocessing
  > 3. data segmentation
  > 4. build models and conduct classification
  > 5. evaluation